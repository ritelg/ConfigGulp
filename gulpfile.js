let gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache'),
    sass = require('gulp-sass'),
    size = require('gulp-size'),
    babel = require('gulp-babel'),
    jshint = require('gulp-jshint'),
    minifycss = require('gulp-minify-css'),
    browserSync = require('browser-sync');


js_src = 'assets/js/**/*.js';
js_dist = 'dist/js/';
scss_src = 'assets/scss/**/*.scss';
scss_dist = 'dist/css/';
img_src = 'assets/img/**/*';
img_dist = 'dist/img/';

gulp.task('browser-sync', function() {
  browserSync({
    server: {
       baseDir: "./"
    }
  });
});

gulp.task('bs-reload', function () {
  browserSync.reload();
});

gulp.task('img', function(){
  gulp.src(img_src)
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest(img_dist));
});

gulp.task('scss', function() {
  gulp.src(scss_src)
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(sass())
    .pipe(autoprefixer('last 2 versions'))
    .pipe(gulp.dest(scss_dist))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(size())
    .pipe(gulp.dest(scss_dist))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('js', function(){
  return gulp.src(js_src)
  .pipe(plumber({
    errorHandler: function (error) {
      console.log(error.message);
      this.emit('end');
  }}))
  .pipe(jshint())
  .pipe(jshint.reporter('default'))
  .pipe(concat('app.js'))
  .pipe(babel({
      presets: ["@babel/preset-env"]
  }))
  .pipe(gulp.dest(js_dist))
  .pipe(rename({suffix: '.min'}))
  .pipe(uglify())
  .pipe(gulp.dest(js_dist))
  .pipe(browserSync.reload({stream:true}))
});

gulp.task('default', ['browser-sync'], function() {
    gulp.watch(scss_src, ['scss']);
    gulp.watch(js_src, ['js']);
    gulp.watch("*.html", ['bs-reload']);
});